package id.onklas.app.api

import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import okhttp3.Interceptor
import okhttp3.Response
import timber.log.Timber
import java.io.IOException
import java.nio.charset.Charset
import javax.inject.Inject

class ResponseInterceptor @Inject constructor(moshi: Moshi) : Interceptor {

    private val errorAdapter by lazy {
        moshi.adapter<Map<String, Any>>(
            Types.newParameterizedType(
                Map::class.java,
                String::class.java,
                Any::class.java
            )
        )
    }

    private val defaultErrorMsg by lazy { "Terjadi gangguan pada koneksi internet Anda, silahkan ulangi beberapa saat lagi" }

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val response = chain.proceed(request)
        val responseCode = response.code
        val responseBody = response.body

        return try {
            val content =
                responseBody?.source()?.also { it.request(Long.MAX_VALUE) }?.buffer?.clone()
                    ?.readString(Charset.forName("UTF-8"))
                    ?: defaultErrorMsg

            if ((responseCode / 100) == 2) {
                val contentMap: Map<String, Any> = try {
                    errorAdapter.fromJson(content) ?: emptyMap()
                } catch (e: Exception) {
                    emptyMap()
                }
                val errorTypes: Array<String> = try {
                    (contentMap["errors"] as? Map<String, *>)?.keys?.toTypedArray()
                        ?: emptyArray()
                } catch (e: Exception) {
                    emptyArray()
                }
                if (contentMap.containsKey("rc") && contentMap["rc"] as? String != "00")
                    throw ApiException(
                        contentMap["rd"] as? String ?: defaultErrorMsg,
                        responseCode,
                        errorTypes
                    )
                else
                    response
            } else {
                val contentMap: Map<String, Any> = try {
                    errorAdapter.fromJson(content) ?: emptyMap()
                } catch (e: Exception) {
                    emptyMap()
                }

                val errorTypes: Array<String> = try {
                    (contentMap["errors"] as? Map<String, *>)?.keys?.toTypedArray()
                        ?: emptyArray()
                } catch (e: Exception) {
                    emptyArray()
                }

                val errorData: Map<String, Any> = try {
                    (contentMap["data"] as? Map<String, Any>) ?: emptyMap()
                } catch (e: Exception) {
                    emptyMap()
                }

                val errorBody = try {
                    "${(contentMap["message"] as? String) ?: ""}. ${(contentMap["error"] as? String) ?: ""}. ${(contentMap["rd"] as? String) ?: ""}"
                } catch (e: Exception) {
                    defaultErrorMsg
                }

                responseBody?.close()
                throw ApiException(errorBody, responseCode, errorTypes, errorData)
            }
        } catch (e: Exception) {
            responseBody?.close()
            Timber.e(e)
            throw e
        }
    }
}

class ApiException(
    override val message: String?,
    val responseCode: Int?,
    val errorTypes: Array<String> = emptyArray(),
    val data: Map<String, Any> = emptyMap()
) : IOException(message)