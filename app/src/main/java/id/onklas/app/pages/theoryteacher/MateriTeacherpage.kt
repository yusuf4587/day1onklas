package id.onklas.app.pages.theoryteacher

import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import id.onklas.app.R
import id.onklas.app.databinding.MateriTeacherPageBinding
import id.onklas.app.di.component
import id.onklas.app.pages.theory.MateriAdapter
import id.onklas.app.pages.theory.MateriDetailPage
import id.onklas.app.pages.theory.TheoryViewModel
import id.onklas.app.utils.LinearSpaceDecoration
import kotlinx.coroutines.launch

class MateriTeacherpage : Fragment() {

    private lateinit var binding: MateriTeacherPageBinding
    private val viewmodel by activityViewModels<TheoryViewModel> { component.theoryVmFactory }
    private var firstLoad = true

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? =
        MateriTeacherPageBinding.inflate(inflater, container, false).also { binding = it }.root

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        binding.swipeRefresh.setOnRefreshListener {
            lifecycleScope.launch {
                firstLoad = true
                viewmodel.prevStartTeacherMateri = -1
                viewmodel.hasNextTeacherMateri = true
                viewmodel.fetchMateriTeacher(0)
                binding.swipeRefresh.isRefreshing = false
            }
        }

        binding.rvMateri.adapter = adapter
        binding.rvMateri.addItemDecoration(
            LinearSpaceDecoration(
                space = resources.getDimensionPixelSize(
                    R.dimen._8sdp
                ), includeTop = true, includeBottom = true
            )
        )

        viewmodel.listTeacherMateri.observe(viewLifecycleOwner, {
            adapter.submitList(it) {
                if (!firstLoad) {
                    val layoutManager =
                        (binding.rvMateri.layoutManager as LinearLayoutManager)
                    val position = layoutManager.findFirstCompletelyVisibleItemPosition()
                    if (position != RecyclerView.NO_POSITION) {
                        binding.rvMateri.scrollToPosition(position)
                    }
                } else {
                    binding.rvMateri.scrollToPosition(0)
                    firstLoad = false
                }
            }
        })
    }

    private val adapter by lazy {
        MateriAdapter(
            { item, view ->
                startActivity(
                    Intent(requireActivity(), MateriDetailPage::class.java)
                        .putExtra("title", item.materi.name)
                        .putExtra("id", item.materi.id.toInt())
                        .putExtra("subId", item.mapel.id.toInt())
                )
            },
            viewmodel.pref.getInt("user_id"),
            { item ->
                AlertDialog.Builder(requireContext(), R.style.DialogTheme)
                    .setItems(arrayOf("Hapus Materi")) { dialog, which ->
                        dialog.dismiss()
                        MaterialAlertDialogBuilder(requireContext(), R.style.DialogTheme)
                            .setMessage("Anda yakin akan menghapus materi?")
                            .setPositiveButton("Hapus") { dialog1, _ ->
                                dialog1.dismiss()
                                lifecycleScope.launch {
                                    val progress =
                                        ProgressDialog.show(
                                            requireContext(),
                                            "",
                                            "menghapus materi"
                                        )
                                    val result = viewmodel.deleteMateri(item.materi.id)
                                    progress.dismiss()
                                    if (result) {
//                                        viewmodel.refreshTimeline(item.feed.feed_type)
                                        viewmodel.db.theory().deleteMateri(item.materi)
                                    }
                                }
                            }
                            .setNeutralButton("Batal") { dialog1, _ -> dialog1.dismiss() }
                            .show()
                    }
                    .show()
            })
    }
}