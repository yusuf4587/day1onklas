package id.onklas.app.pages.theory

import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.lifecycle.lifecycleScope
import es.voghdev.pdfviewpager.library.adapter.PDFPagerAdapter
import id.onklas.app.databinding.PdfPageBinding
import id.onklas.app.di.component
import id.onklas.app.pages.Privatepage
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.io.File
import java.io.FileOutputStream

class PdfPage : Privatepage() {

    private val binding by lazy { PdfPageBinding.inflate(layoutInflater) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(binding.root)
        setSupportActionBar(binding.toolbar)
        val titleText = intent.getStringExtra("title") ?: "Baca PDF"
        supportActionBar?.apply {
            setDisplayShowHomeEnabled(true)
            setDisplayHomeAsUpEnabled(true)

            binding.toolbar.setNavigationOnClickListener { finish() }
            binding.toolbar.title = titleText
            title = titleText
        }

        intent.getStringExtra("file_path")?.let {
            toast("Sedang membuka file")
            val uri = Uri.parse(it)
            val file = File(filesDir, "${uri.lastPathSegment ?: titleText}.pdf")
            Timber.e("file path: ${file.absolutePath}")

            if (file.exists()) {
                binding.progress.visibility = View.GONE
                binding.vpPdf.adapter = PDFPagerAdapter(this, file.path)
            } else {
                val errorMsg =
                    "Gangguan koneksi ketika membuka file, silahkan ulangi beberapa saat lagi"
                try {
                    lifecycleScope.launchWhenCreated {
                        val download = component.apiService.download(it)
//                        withContext(Dispatchers.IO) {
                        FileOutputStream(file).apply {
                            write(download.bytes())
                            flush()
                            close()
//                            }
                        }

                        binding.progress.visibility = View.GONE
                        binding.vpPdf.adapter =
                            PDFPagerAdapter(this@PdfPage, file.absolutePath)
                    }
                } catch (e: Exception) {
                    Timber.e(e)
                    alert(
                        msg = errorMsg,
                        okClick = this::finish
                    )
                }
            }
        }
    }
}