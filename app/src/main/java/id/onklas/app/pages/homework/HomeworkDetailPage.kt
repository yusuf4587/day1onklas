package id.onklas.app.pages.homework

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import droidninja.filepicker.FilePickerConst
import droidninja.filepicker.utils.ContentUriUtils
import id.onklas.app.R
import id.onklas.app.databinding.HomeworkDetailPageBinding
import id.onklas.app.di.component
import id.onklas.app.pages.Privatepage
import id.onklas.app.utils.IntentUtil
import kotlinx.coroutines.launch
import java.io.File

class HomeworkDetailPage : Privatepage() {

    private val binding by lazy { HomeworkDetailPageBinding.inflate(layoutInflater) }
    private lateinit var item: HomeworkItemTable
    private val viewmodel by viewModels<HomeWorkViewModel> { component.homeworkVmFactory }
    private val instruction by lazy { ArrayList<Pair<String, String>>() }
    private val instDownload by lazy { "download" to "Silahkan baca/download soal terlebih dahulu" }
    private val instUpload by lazy { "upload" to "Upload file jawaban untuk menyelesaikan tugas" }

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(binding.root)
        setSupportActionBar(binding.toolbar)
        supportActionBar?.apply {
            setDisplayShowHomeEnabled(true)
            setDisplayHomeAsUpEnabled(true)

            binding.toolbar.setNavigationOnClickListener { supportFinishAfterTransition() }
        }

        lifecycleScope.launchWhenResumed {
            loading(msg = "menampilkan detail tugas")
            if (intent.hasExtra("id") && intent.getIntExtra("id", 0) > 0) {
                val id = intent.getIntExtra("id", 0)
                val isFinished = intent.getBooleanExtra("isFinished", false)
                viewmodel.getDetailHomework(id)?.let {
                    val item = it
                    val isStudent = viewmodel.pref.getBoolean("is_student")
                    binding.isStudent = isStudent
                    binding.item = item
                    val allowUpload =
                        item.homework.uploaded == 1
                                && !item.homework.is_overdue
                                && !isFinished
                                && isStudent
                    binding.allowUpload = allowUpload
                    binding.isFinished = isFinished

                    if (isFinished) {
                        binding.info = "Kamu sudah mengerjakan tugas"
                        binding.infoColor =
                            ContextCompat.getColor(this@HomeworkDetailPage, R.color.colorPrimary)
                    } else if (item.homework.is_overdue) {
                        binding.info = "Waktu pengumpulan tugas sudah berakhir"
                        binding.infoColor =
                            ContextCompat.getColor(this@HomeworkDetailPage, R.color.red)
                    }

                    // set detail soal
                    if (item.homework.file_path.isNotEmpty()) {
                        val uri = Uri.parse(item.homework.file_path.replace("\\", ""))
                        val size =
                            if (item.homework.file_size.isEmpty()) 0L else item.homework.file_size.toLong()
                        binding.materiDetailLayout.name.text =
                            "${item.homework.file_name} | ${viewmodel.fileUtils.getStringSizeLengthFile(
                                size
                            )}"
                        binding.materiDetailLayout.btnLihat.text = "baca tugas"
                        binding.materiDetailLayout.btnLihat.setOnClickListener {
                            viewmodel.intentUtil.openPdf(
                                this@HomeworkDetailPage,
                                uri.toString(),
                                item.homework.title
                            )
                            instruction.remove(instDownload)
                        }
                        binding.materiDetailLayout.btnDownload.setOnClickListener {
                            viewmodel.intentUtil.downloadFile(
                                this@HomeworkDetailPage,
                                uri,
                                "${item.homework.file_name}.pdf",
                                "soal"
                            )
                            instruction.remove(instDownload)
                        }
                    }

                    // set jawaban layout
                    if (item.homework.file_student_path.isNotEmpty()) {
                        val uri = Uri.parse(item.homework.file_student_path.replace("\\", ""))
                        val size =
                            if (item.homework.file_student_size.isEmpty()) 0L else item.homework.file_student_size.toLong()
                        binding.answerLayout.name.text =
                            "${item.homework.file_student_name} | ${viewmodel.fileUtils.getStringSizeLengthFile(
                                size
                            )}"
                        binding.answerLayout.btnLihat.text = "baca jawaban"
                        binding.answerLayout.btnLihat.setOnClickListener {
                            viewmodel.intentUtil.openPdf(
                                this@HomeworkDetailPage,
                                uri.toString(),
                                item.homework.file_student_name
                            )
                        }
                        binding.answerLayout.btnDownload.setOnClickListener {
                            viewmodel.intentUtil.downloadFile(
                                this@HomeworkDetailPage,
                                uri,
                                "${item.homework.file_student_name}.pdf",
                                "jawaban"
                            )
                        }
                    }

                    // set upload layout
                    if (allowUpload) {
                        binding.materiUploadLayout.btnFile.setOnClickListener {
                            viewmodel.intentUtil.openFilePicker(
                                this@HomeworkDetailPage,
                                "Upload File Tugas",
                                listOf(
                                    Triple("Pdf", arrayOf("pdf"), R.drawable.ic_pdf),
                                    Triple("Gambar", arrayOf("png", "jpg", "jpeg"), R.drawable.image_placeholder)
                                )
                            )
                        }
                    }

                    // set button finish
                    if (isStudent) {
                        // set allow finish homework
                        if (item.homework.downloded > 0) {
                            instruction.add(instDownload)
                        }
                        if (item.homework.uploaded > 0) {
                            instruction.add(instUpload)
                            viewmodel.tugasFile.observe(this@HomeworkDetailPage, Observer {
                                if (!it.isNullOrEmpty())
                                    instruction.remove(instUpload)
                            })
                        }

                        binding.btnUpload.setOnClickListener {
                            lifecycleScope.launch {
                                if (instruction.isEmpty()) {
                                    loading("sedang mengumpulkan tugas")
                                    val res = viewmodel.uploadHomework(item.homework.id)
                                    dismissloading()
                                    if (res)
                                        finish()
                                } else {
                                    toast(instruction.joinToString(". ") { it.second })
                                }
                            }
                        }
                    }
                    binding.executePendingBindings()
                }
            } else {
                alert(msg = "Halaman tidak tersedia", okClick = {
                    finish()
                })
            }
            dismissloading()
        }

        viewmodel.errorString.observe(this, Observer(this::toast))
    }

    @SuppressLint("SetTextI18n")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            IntentUtil.RC_PDF_PICKER -> {
                val uri =
                    data?.getParcelableArrayListExtra<Uri>(FilePickerConst.KEY_SELECTED_DOCS)
                if (!uri.isNullOrEmpty()) {
                    ContentUriUtils.getFilePath(this, uri.first())?.let {
                        viewmodel.tugasFile.postValue(it)
                        val file = File(it)
                        binding.materiUploadLayout.icPdf.visibility = View.VISIBLE
                        binding.materiUploadLayout.uploadFileInfo.text =
                            "${file.name} (ukuran: ${viewmodel.fileUtils.getStringSizeLengthFile(
                                file.length()
                            )})"
                    }
                }
            }
            else -> super.onActivityResult(requestCode, resultCode, data)
        }
    }
}