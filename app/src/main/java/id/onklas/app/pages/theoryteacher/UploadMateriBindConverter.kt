package id.onklas.app.pages.theoryteacher

import androidx.databinding.InverseMethod
import id.onklas.app.pages.theory.GradeTable
import id.onklas.app.pages.theory.MapelTable

object UploadMateriBindConverter {

    val listGrade by lazy { ArrayList<GradeTable>() }
    val listSubject by lazy { ArrayList<MapelTable>() }

    @JvmStatic
    fun gradeId_to_name(gradeId: Int): String? =
        listGrade.firstOrNull { it.id == gradeId }?.name

    @JvmStatic
    @InverseMethod(value = "gradeId_to_name")
    fun name_to_gradeId(name: String): Int? =
        listGrade.firstOrNull { it.name == name }?.id

    @JvmStatic
    fun subjectId_to_name(subjectId: Int): String? =
        listSubject.firstOrNull { it.id.toInt() == subjectId }?.name

    @JvmStatic
    @InverseMethod(value = "subjectId_to_name")
    fun name_to_subjectId(name: String): Int? =
        listSubject.firstOrNull { it.name == name }?.id?.toInt()
}