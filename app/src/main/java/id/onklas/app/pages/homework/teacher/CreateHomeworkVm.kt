package id.onklas.app.pages.homework.teacher

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.github.kittinunf.fuel.core.FileDataPart
import com.github.kittinunf.fuel.core.extensions.jsonBody
import com.github.kittinunf.fuel.coroutines.awaitObjectResult
import com.github.kittinunf.fuel.httpPost
import com.github.kittinunf.fuel.httpUpload
import com.github.kittinunf.fuel.moshi.moshiDeserializerOf
import com.squareup.moshi.Moshi
import id.onklas.app.api.ApiService
import id.onklas.app.db.MemoryDB
import id.onklas.app.db.OnKlasDbUtil
import id.onklas.app.pages.homework.ClassRoomTable
import id.onklas.app.pages.homework.HomeworkCreateResponse
import id.onklas.app.pages.login.SekolahItem
import id.onklas.app.pages.theory.MapelTable
import id.onklas.app.pages.theory.MapelTeacherCrossRef
import id.onklas.app.pages.theory.TeacherItem
import id.onklas.app.pages.theoryteacher.UploadMateriBindConverter
import id.onklas.app.utils.ApiWrapper
import id.onklas.app.utils.FileUtils
import id.onklas.app.utils.IntentUtil
import id.onklas.app.utils.PreferenceClass
import kotlinx.coroutines.launch
import timber.log.Timber
import java.io.File
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class CreateHomeworkVm @Inject constructor(
    val pref: PreferenceClass,
    val moshi: Moshi,
    val db: MemoryDB,
    val dbUtil: OnKlasDbUtil,
    val fileUtils: FileUtils,
    val intentUtil: IntentUtil,
    val apiWrapper: ApiWrapper,
    val apiService: ApiService
) : ViewModel() {

    val teacher by lazy {
        try {
            moshi.adapter(TeacherItem::class.java).fromJson(pref.getString("teacher"))
        } catch (e: Exception) {
            null
        }
    }
    val school: SekolahItem by lazy {
       try { moshi.adapter(SekolahItem::class.java).fromJson(pref.getString("school")) ?: SekolahItem() } catch (e: Exception) { SekolahItem() }
    }
    val errorString by lazy { MutableLiveData<String>() }
    val urlApi by lazy { pref.getString("url_api") }
    val anyAdapter by lazy { moshi.adapter(Any::class.java) }
    val dateFormat by lazy { SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale("id")) }

    val listMapel by lazy { MutableLiveData<List<MapelTable>>() }
    val listClass by lazy { MutableLiveData<List<ClassRoomTable>>() }

    val mapel by lazy { MutableLiveData<Int>() }
    val classId by lazy { MutableLiveData<Int>() }
    val expired by lazy { MutableLiveData<String>(dateFormat.format(Date(System.currentTimeMillis()))) }
    val name by lazy { MutableLiveData<String>() }
    val desc by lazy { MutableLiveData<String>() }
    val file by lazy { MutableLiveData<String>() }
    val allowCreate by lazy { MutableLiveData<Boolean>(false) }
    val uploadFile by lazy { MutableLiveData<Boolean>(false) }
    val needDownload by lazy { MutableLiveData<Boolean>(false) }
    val needUpload by lazy { MutableLiveData<Boolean>(false) }

    suspend fun fetchClassRoom(): List<ClassRoomTable> = try {
        apiService.teacherClassRoom()
//        "${urlApi}mobile/teacher/school-class-room"
//            .httpGet()
//            .awaitObjectResult(moshiDeserializerOf(moshi.adapter(ClassRoomResponse::class.java)))
//            .get()
            .data
            .also { db.homework().insertClassRoom(it) }
    } catch (e: Exception) {
        Timber.e(e)
        errorString.postValue(e.localizedMessage)
        emptyList()
    }

    private suspend fun fetchMapel(): List<MapelTable> = try {
        apiService.teacherSubjectTeach()
//        "${urlApi}mobile/teacher/school-subject"
//            .httpGet()
//            .awaitObjectResult(moshiDeserializerOf(moshi.adapter(MapelResponse::class.java)))
//            .get()
            .data.let {
                it.map {
                    db.theory().insertMapelTeacherCrossRef(
                        MapelTeacherCrossRef(
                            it.id.toLong(),
                            teacher?.id?.toLong() ?: 0
                        )
                    )
                    MapelTable(
                        it.id.toLong(),
                        it.name,
                        it.icon_image,
                        it.message_label,
                        it.teacher.id,
                        it.teacher.user.id
                    )
                }.also {
                    db.theory().insertMapel(it)
                }
            }
    } catch (e: Exception) {
        Timber.e(e)
        emptyList()
    }

    suspend fun create(): Boolean = try {
        val homework = "${urlApi}mobile/teacher/school-subject/${mapel.value}/assignment"
            .httpPost()
            .jsonBody(
                anyAdapter.toJson(
                    mapOf(
                        "title" to name.value,
                        "description" to desc.value,
                        "teacher_id" to teacher?.id,
                        "school_id" to school.id,
                        "school_subject_id" to mapel.value,
                        "school_classes_id" to classId.value,
                        "school_subject_schedules_id" to 5,
                        "grade" to CreateHomeworkBindConverter.classId_to_grade(classId.value),
                        "end_at" to expired.value,
                        "checked" to 1,
                        "downloded" to if (needDownload.value == true) 1 else 0,
                        "uploaded" to if (needUpload.value == true) 1 else 0
                    )
                )
            )
            .awaitObjectResult(moshiDeserializerOf(moshi.adapter(HomeworkCreateResponse::class.java)))
            .get().data

        file.value?.let { filepath ->
            "${urlApi}mobile/teacher/school-subject/${mapel.value}/assignment/${homework.id}/file"
                .httpUpload()
                .add { FileDataPart(File(filepath), name = "file") }
                .awaitObjectResult(moshiDeserializerOf(anyAdapter))
                .get()
        }
        true
    } catch (e: Exception) {
        Timber.e(e)
        errorString.postValue(e.localizedMessage)
        false
    }

    init {
        viewModelScope.launch {
            val listClassRoom = db.homework().getListClassroom().let {
                if (it.isEmpty()) fetchClassRoom() else it
            }
            CreateHomeworkBindConverter.listClass.addAll(listClassRoom)
            listClass.postValue(listClassRoom)

            val listMapelUpdate = db.theory().getListMapel().let {
                if (it.isEmpty()) fetchMapel() else it
            }
            UploadMateriBindConverter.listSubject.addAll(listMapelUpdate)
            listMapel.postValue(listMapelUpdate)
        }
    }
}