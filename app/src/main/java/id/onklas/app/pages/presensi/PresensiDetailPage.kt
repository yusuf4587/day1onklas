package id.onklas.app.pages.presensi

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.OrientationHelper
import id.onklas.app.databinding.PresensiDetailPageBinding
import id.onklas.app.di.component
import id.onklas.app.pages.pembayaran.RowAdapter
import id.onklas.app.pages.PageDialogFragment
import kotlinx.coroutines.launch

class PresensiDetailPage(val scheduleId: Int, val date: String) : PageDialogFragment() {

    private lateinit var binding: PresensiDetailPageBinding
    private val viewmodel by activityViewModels<PresensiViewModel> { component.presensiVmFactory }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? =
        PresensiDetailPageBinding.inflate(inflater, container, false).also { binding = it }.root

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        binding.toolbar.setNavigationOnClickListener { dismiss() }

        binding.rvInfo.addItemDecoration(
            DividerItemDecoration(
                requireContext(),
                OrientationHelper.VERTICAL
            )
        )

        binding.swipeRefresh.setOnRefreshListener(this::init)
        binding.swipeRefresh.isRefreshing = true
        init()
    }

    private fun init() {
        lifecycleScope.launch {
            viewmodel.detailClass(scheduleId, date).observe(viewLifecycleOwner, observer)
        }
    }

    private val observer by lazy {
        Observer<ScheduleDetailTable> { item ->
            try {
                binding.swipeRefresh.isRefreshing = false
                binding.item = item
                binding.rvInfo.adapter = RowAdapter(
                    arrayListOf(
                        "Pengajar" to item.teacher_name,
                        "Kelas" to item.class_name + " " + item.school_major_name,
                        "Sekolah" to item.school_name,
                        "Jadwal" to item.plot_start_at + " - " + item.plot_end_at,
                        "Waktu Mulai" to item.attend_at,
                        "Waktu Selesai" to item.leave_at,
                        "Password" to item.class_password,
                        "Kehadiran" to "${item.total_attended_student}/${item.total_student}"
                    )
                )
                binding.executePendingBindings()
            } catch (e: Exception) {
            }
        }
    }
}