package id.onklas.app.pages.theoryteacher

import android.net.Uri
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.squareup.moshi.Moshi
import id.onklas.app.api.ApiService
import id.onklas.app.db.MemoryDB
import id.onklas.app.pages.theory.GradeTable
import id.onklas.app.pages.theory.MapelTable
import id.onklas.app.pages.theory.MapelTeacherCrossRef
import id.onklas.app.pages.theory.TeacherItem
import id.onklas.app.utils.ApiWrapper
import id.onklas.app.utils.FileUtils
import id.onklas.app.utils.IntentUtil
import id.onklas.app.utils.PreferenceClass
import kotlinx.coroutines.launch
import timber.log.Timber
import java.io.File
import javax.inject.Inject

class UploadMateriViewmodel @Inject constructor(
    val pref: PreferenceClass,
    val moshi: Moshi,
    val db: MemoryDB,
    val fileUtils: FileUtils,
    val intentUtil: IntentUtil,
    val apiService: ApiService,
    val apiWrapper: ApiWrapper
) : ViewModel() {
    val errorString by lazy { MutableLiveData<String>() }
    val allowUpload by lazy { MutableLiveData<Boolean>(false) }
    val materiTitle by lazy { MutableLiveData<String>("") }
    val subjectId by lazy { MutableLiveData<Int>() }
    val grade by lazy { MutableLiveData<Int>() }
    val materiFile by lazy { MutableLiveData<String>() }

    val listSubject by lazy { MutableLiveData<List<MapelTable>>() }
    val listGrade by lazy { MutableLiveData<List<GradeTable>>() }

    val teacher by lazy {
        try {
            moshi.adapter(TeacherItem::class.java).fromJson(pref.getString("teacher"))
        } catch (e: Exception) {
            null
        }
    }

    private suspend fun fetchGrade(): List<GradeTable> = try {
        apiService.gradeTeach().data.also { db.theory().insertGrade(it) }
    } catch (e: Exception) {
        Timber.e(e)
        emptyList()
    }

    private suspend fun fetchMapel(): List<MapelTable> = try {
        apiService.teacherSubjectTeach()
            .data.let {
                it.map {
                    db.theory().insertMapelTeacherCrossRef(
                        MapelTeacherCrossRef(
                            it.id.toLong(),
                            teacher?.id?.toLong() ?: 0
                        )
                    )
                    MapelTable(
                        it.id.toLong(),
                        it.name,
                        it.icon_image,
                        it.message_label,
                        it.teacher.id,
                        it.teacher.user.id
                    )
                }.also {
                    db.theory().insertMapel(it)
                }
            }
    } catch (e: Exception) {
        Timber.e(e)
        emptyList()
    }

    suspend fun createMateri(): Boolean = try {
        apiWrapper.createTheory(
            materiTitle.value.orEmpty(),
            grade.value ?: 0,
            subjectId.value ?: 0,
            File(Uri.parse(materiFile.value)?.path.orEmpty())
        )
        true
    } catch (e: Exception) {
        Timber.e(e)
        errorString.postValue(e.localizedMessage ?: "Gagal membuat materi")
        false
    }

    init {
        viewModelScope.launch {
            UploadMateriBindConverter.listSubject.clear()
            UploadMateriBindConverter.listSubject.addAll(
//                db.theory().getListTeacherMapel(teacher?.id?.toInt() ?: 0).let {
//                    if (it.isEmpty())
                fetchMapel()
//                    else it.map { it.mapel }
//                }
            )
            listSubject.postValue(UploadMateriBindConverter.listSubject)

            UploadMateriBindConverter.listGrade.clear()
            UploadMateriBindConverter.listGrade.addAll(db.theory().getListGrade().let {
                if (it.isEmpty()) fetchGrade()
                else it
            })
            listGrade.postValue(UploadMateriBindConverter.listGrade)
        }
    }
}