package id.onklas.app.pages.login

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import androidx.activity.viewModels
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.app.Person
import androidx.core.app.RemoteInput
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.IconCompat
import androidx.lifecycle.lifecycleScope
import androidx.work.ExistingPeriodicWorkPolicy
import androidx.work.PeriodicWorkRequestBuilder
import androidx.work.WorkManager
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import id.onklas.app.BuildConfig
import id.onklas.app.R
import id.onklas.app.databinding.LoginPageBinding
import id.onklas.app.di.component
import id.onklas.app.pages.PublicPage
import id.onklas.app.pages.chat.ChatPage
import id.onklas.app.pages.home.HomePage
import id.onklas.app.pages.onboard.OnBoardPage
import id.onklas.app.services.DirectReplyChat
import id.onklas.app.viewmodels.GeneralViewModel
import id.onklas.app.worker.LogUploader
import java.util.concurrent.TimeUnit

class Loginpage : PublicPage() {

    private val binding by lazy { LoginPageBinding.inflate(layoutInflater) }
    private val viewmodel by viewModels<GeneralViewModel> { component.generalVmFactory }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

//        // testing notif
//
//        // create sender person
//        val personWith = Person.Builder()
//            .setName("yee yee ass")
//            .setIcon(
//                IconCompat.createWithResource(this, R.mipmap.ic_launcher_circle)
//            )
//            .setUri("yeyeye")
//            .build()
//
//        // create user person
//        val personMe = Person.Builder()
//            .setName("Anda")
//            .setIcon(
//                IconCompat.createWithResource(this, R.drawable.ic_keyboard)
//            )
//            .setUri("mememe")
//            .build()
//
//        // build remote input for direct reply
//        val remoteInput = RemoteInput.Builder("reply_message").run {
//            setLabel("Tulis pesan...")
//            build()
//        }
//
//        // build main pending intent for handler notif click
//        val mainPendingIntent = PendingIntent.getActivity(
//            applicationContext,
//            0,
//            Intent(this, ChatPage::class.java),
//            PendingIntent.FLAG_UPDATE_CURRENT
//        )
//
//        // Build a PendingIntent for the reply action to trigger.
//        val replyPendingIntent = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//            // if api level 24+, get service to direct reply from notif
//            PendingIntent.getService(
//                applicationContext,
//                1,
//                Intent(this, DirectReplyChat::class.java).putExtra("key", "value"),
//                PendingIntent.FLAG_UPDATE_CURRENT
//            )
//        } else {
//            // use main notif intent
//            mainPendingIntent
//        }
//
//        // build notif action for direct reply
//        val replyAction = NotificationCompat.Action.Builder(
//            android.R.drawable.ic_menu_send,
//            "Balas",
//            replyPendingIntent
//        )
//            .addRemoteInput(remoteInput)
//            .setShowsUserInterface(false)
//            .setSemanticAction(NotificationCompat.Action.SEMANTIC_ACTION_REPLY)
//            .build()
//
//        // build messaging style
//        val messagingStyle = NotificationCompat.MessagingStyle(personMe)
//            .setConversationTitle("2 pesan baru dari yee yee")
//            .addMessage(
//                "pesan 1",
//                System.currentTimeMillis() + 2,
//                personWith
//            )
//            .addMessage(
//                "pesan 2",
//                System.currentTimeMillis(),
//                personWith
//            )
//            .setGroupConversation(false)
//
//        // create notification chat
//        val chatNotif =
//            NotificationCompat.Builder(applicationContext, getString(R.string.app_name))
//                .setSmallIcon(R.drawable.ic_logo_notif)
//                .setContentTitle("yee yee ass 1")
//                .setContentText("2 pesan baru")
//                .setStyle(messagingStyle)
//                .setContentIntent(mainPendingIntent)
//                .setDefaults(NotificationCompat.DEFAULT_ALL)
//                .setColor(ContextCompat.getColor(applicationContext, R.color.colorPrimary))
//                .addAction(replyAction)
//                .setCategory(Notification.CATEGORY_MESSAGE)
//                .setVisibility(NotificationCompat.VISIBILITY_PRIVATE)
//                .setGroup("chat_group")
//
//        val chatNotif2 =
//            NotificationCompat.Builder(applicationContext, getString(R.string.app_name))
//                .setSmallIcon(R.drawable.ic_logo_notif)
//                .setContentTitle("yee yee ass 2")
//                .setContentText("3 pesan baru")
//                .setStyle(messagingStyle)
//                .setContentIntent(mainPendingIntent)
//                .setDefaults(NotificationCompat.DEFAULT_ALL)
//                .setColor(ContextCompat.getColor(applicationContext, R.color.colorPrimary))
//                .addAction(replyAction)
//                .setCategory(Notification.CATEGORY_MESSAGE)
//                .setVisibility(NotificationCompat.VISIBILITY_PRIVATE)
//                .setGroup("chat_group")
//
//        val chatGroupNotif =
//            NotificationCompat.Builder(applicationContext, getString(R.string.app_name))
//                .setSmallIcon(R.drawable.ic_logo_notif)
//                .setContentTitle("Pesan")
//                .setContentText("4 pesan baru")
//                .setStyle(NotificationCompat.InboxStyle()
//                    .addLine("pesan 1")
//                    .addLine("pesan 2")
//                    .addLine("pesan 3")
//                    .addLine("pesan 4")
//                    .setSummaryText("4 pesan baru")
//                )
//                .setContentIntent(mainPendingIntent)
//                .setDefaults(NotificationCompat.DEFAULT_ALL)
//                .setColor(ContextCompat.getColor(applicationContext, R.color.colorPrimary))
//                .setVisibility(NotificationCompat.VISIBILITY_PRIVATE)
//                .setGroup("chat_group")
//                .setGroupSummary(true)
//
//        // show notif
//        NotificationManagerCompat.from(applicationContext).notify(1, chatNotif.build())
//        NotificationManagerCompat.from(applicationContext).notify(2, chatNotif2.build())
//        NotificationManagerCompat.from(applicationContext).notify(3, chatGroupNotif.build())
//        return

        lifecycleScope.launchWhenCreated {
            // setNotificationChannel
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val name = getString(R.string.app_name)

                // Register the channel with the system
                (getSystemService(Context.NOTIFICATION_SERVICE) as? NotificationManager)?.apply {

                    // create default notification channel
                    createNotificationChannel(
                        NotificationChannel(
                            name,
                            name,
                            NotificationManager.IMPORTANCE_DEFAULT
                        ).apply {
                            description = name
                        }
                    )

                    // create silent notification channel
                    createNotificationChannel(
                        NotificationChannel(
                            "${name}.silent",
                            "${name}.silent",
                            NotificationManager.IMPORTANCE_MIN
                        ).apply {
                            description = name
                            setSound(null, null)
                            enableLights(false)
                            enableVibration(false)
                        }
                    )
                }
            }

            // run periodic worker to upload log file
            WorkManager.getInstance(applicationContext)
                .enqueueUniquePeriodicWork(
                    "upload_log",
                    ExistingPeriodicWorkPolicy.KEEP,
                    PeriodicWorkRequestBuilder<LogUploader>(6, TimeUnit.HOURS).build()
                )

            // check current apps version
            viewmodel.checkVersion()?.let {
                val latestVersion = it.version.split(".").map { it.toInt() }
                val currentVersion = BuildConfig.VERSION_NAME.split(".").map { it.toInt() }

                val needUpdate = latestVersion
                    .zip(currentVersion)
                    .firstOrNull { it.first != it.second }?.let { it.first > it.second }
                    ?: false

                if (needUpdate) {
                    MaterialAlertDialogBuilder(
                        this@Loginpage,
                        R.style.DialogTheme
                    )
                        .setTitle("Update Tersedia")
                        .setMessage("Silahkan update ke versi terbaru aplikasi")
                        .setCancelable(false)
                        .setPositiveButton("Update") { dialog, _ ->
                            // try to open play store
                            try {
                                startActivity(
                                    Intent(
                                        Intent.ACTION_VIEW,
                                        Uri.parse("market://details?id=$packageName")
                                    )
                                )
                            } catch (anfe: ActivityNotFoundException) {
                                startActivity(
                                    Intent(
                                        Intent.ACTION_VIEW,
                                        Uri.parse("https://play.google.com/store/apps/details?id=$packageName")
                                    )
                                )
                            }
                            finish()
                        }
                        .show()
                } else goNext()
            } ?: goNext()
        }
    }

    private fun goNext() {
        if (!viewmodel.pref.getBoolean("onboard", false)) {
            startActivity(Intent(this@Loginpage, OnBoardPage::class.java))
            finish()
        } else if (viewmodel.pref.getBoolean("logged_in")) {
            // try to open page based on params/extra "goto"
            try {
                startActivity(
                    Intent(
                        this@Loginpage,
                        Class.forName(intent.getStringExtra("goto"))
                    )
                )
            } catch (e: Exception) {
                startActivity(Intent(this@Loginpage, HomePage::class.java))
            }
            finish()
        } else {
            window.setBackgroundDrawable(null)
            setContentView(binding.root)
        }
    }
}