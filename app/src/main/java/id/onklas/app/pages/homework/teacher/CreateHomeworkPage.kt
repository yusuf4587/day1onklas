package id.onklas.app.pages.homework.teacher

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.core.content.res.ResourcesCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import com.google.android.material.datepicker.CalendarConstraints
import com.google.android.material.datepicker.MaterialDatePicker
import com.google.android.material.timepicker.MaterialTimePicker
import com.google.android.material.timepicker.TimeFormat
import com.linkdev.filepicker.factory.IPickFilesFactory
import com.linkdev.filepicker.interactions.PickFilesStatusCallback
import com.linkdev.filepicker.models.ErrorModel
import com.linkdev.filepicker.models.FileData
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog
import droidninja.filepicker.FilePickerConst
import droidninja.filepicker.utils.ContentUriUtils
import id.onklas.app.R
import id.onklas.app.databinding.CreateHomeworkPageBinding
import id.onklas.app.di.component
import id.onklas.app.pages.Privatepage
import id.onklas.app.utils.IntentUtil
import id.onklas.app.utils.NoFilterArrayAdapter
import kotlinx.coroutines.launch
import timber.log.Timber
import java.io.File
import java.util.*

class CreateHomeworkPage : Privatepage() {

    private val binding by lazy { CreateHomeworkPageBinding.inflate(layoutInflater) }
    private val viewmodel by viewModels<CreateHomeworkVm> { component.createHomeworkVmFactory }
    private val colorPrimary by lazy {
        ResourcesCompat.getColor(
            resources,
            R.color.colorPrimary,
            null
        )
    }
    private var pickFilesFactory: IPickFilesFactory? = null

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(binding.root)
        setSupportActionBar(binding.toolbar)
        supportActionBar?.apply {
            setDisplayShowHomeEnabled(true)
            setDisplayHomeAsUpEnabled(true)

            binding.toolbar.setNavigationOnClickListener { finish() }
        }

        binding.lifecycleOwner = this
        viewmodel.apply {
            binding.viewmodel = this
            listMapel.observe(this@CreateHomeworkPage, androidx.lifecycle.Observer {
                binding.inputMapel.setAdapter(
                    NoFilterArrayAdapter(
                        this@CreateHomeworkPage,
                        R.layout.text_item,
                        it.map { it.name })
                )
            })

            listClass.observe(this@CreateHomeworkPage, androidx.lifecycle.Observer {
                binding.inputKelas.setAdapter(
                    NoFilterArrayAdapter(
                        this@CreateHomeworkPage,
                        R.layout.text_item,
                        it.map { it.name })
                )
            })

            allowCreate.observe(this@CreateHomeworkPage, Observer(binding.btnPost::setEnabled))

            name.observe(this@CreateHomeworkPage, allowPostObserver)
            name.observe(this@CreateHomeworkPage, Observer {
                binding.titleCounter.text = "${if (it.isNullOrEmpty()) "0" else it.length}/500"
            })

            mapel.observe(this@CreateHomeworkPage, allowPostObserver)
            classId.observe(this@CreateHomeworkPage, allowPostObserver)
            file.observe(this@CreateHomeworkPage, allowPostObserver)
            file.observe(this@CreateHomeworkPage, Observer {
                binding.checkStudentDownload.visibility =
                    if (it.isNullOrEmpty()) View.GONE else View.VISIBLE
            })

            uploadFile.observe(this@CreateHomeworkPage, allowPostObserver)
            uploadFile.observe(this@CreateHomeworkPage, Observer<Boolean> {
                val visibility = if (it) View.VISIBLE else View.GONE
                binding.fileInfo.visibility = visibility
                binding.materiUploadLayout.root.visibility = visibility
            })

            errorString.observe(this@CreateHomeworkPage, Observer(this@CreateHomeworkPage::toast))
        }

        binding.inputDuedate.setOnClickListener {

            val calendar = Calendar.getInstance()
            DatePickerDialog.newInstance { view, year, monthOfYear, dayOfMonth ->
                view.dismiss()
                TimePickerDialog.newInstance({ view, hourOfDay, minute, second ->
                    view.dismiss()
                    calendar.set(year, monthOfYear, dayOfMonth, hourOfDay, minute, second)

                    val date = viewmodel.dateFormat.format(calendar.time)
                    Timber.e("data: $date")

                    binding.inputDuedate.setText(date)
                    viewmodel.expired.postValue(date)
                }, true).apply {
                    accentColor = colorPrimary
                    title = "Batas pengumpulan"
                    show(supportFragmentManager, "time_picker")
                }
            }
                .apply {
                    accentColor = colorPrimary
                    minDate = calendar
                    title = "Batas pengumpulan"
                    show(supportFragmentManager, "date_picker")
                }

//            MaterialDatePicker.Builder.datePicker()
//                .setTheme(R.style.AppCalendar)
//                .setTitleText("Batas pengumpulan tugas")
//                .setSelection(System.currentTimeMillis())
//                .setCalendarConstraints(
//                    CalendarConstraints.Builder()
//                        .setStart(System.currentTimeMillis())
//                        .build()
//                )
//                .build()
//                .apply {
//                    addOnPositiveButtonClickListener {
//                        val calendar = Calendar.getInstance().apply { timeInMillis = it }
//
//                        MaterialTimePicker.Builder()
//                            .setTimeFormat(TimeFormat.CLOCK_24H)
//                            .setHour(0)
//                            .setMinute(0)
//                            .setTitleText("Batas pengumpulan tugas")
//                            .build()
//                            .apply {
//                                show(supportFragmentManager, "time_picker")
//                                addOnPositiveButtonClickListener {
//                                    calendar.set(Calendar.HOUR, hour)
//                                    calendar.set(Calendar.MINUTE, minute)
//
//                                    val date = viewmodel.dateFormat.format(calendar.time)
//                                    Timber.e("data: $date")
//
//                                    binding.inputDuedate.setText(date)
//                                    viewmodel.expired.postValue(date)
//                                }
//                            }
//                    }
//                }
//                .show(supportFragmentManager, "date_picker")
        }

        binding.materiUploadLayout.btnFile.setOnClickListener {
            pickFilesFactory = viewmodel.intentUtil.openFilePicker2(this, "Pilih File Tugas")
        }

        binding.btnPost.setOnClickListener {
            lifecycleScope.launch {
                loading(msg = "sedang membuat tugas")
                val res = viewmodel.create()
                dismissloading()
                if (res)
                    finish()
            }
        }

        binding.executePendingBindings()
    }

    private val allowPostObserver by lazy {
        Observer { it: Any ->
            viewmodel.allowCreate.postValue(
                !viewmodel.name.value.isNullOrEmpty() &&
                        viewmodel.mapel.value ?: 0 > 0 &&
                        viewmodel.classId.value ?: 0 > 0
//                        &&
//                        (viewmodel.uploadFile.value == true && !viewmodel.file.value.isNullOrEmpty())
            )
            if (viewmodel.uploadFile.value == true)
                viewmodel.allowCreate.postValue(!viewmodel.file.value.isNullOrEmpty())
        }
    }

    @SuppressLint("SetTextI18n")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            IntentUtil.RC_PDF_PICKER -> {
                val uri =
                    data?.getParcelableArrayListExtra<Uri>(FilePickerConst.KEY_SELECTED_DOCS)
                if (!uri.isNullOrEmpty()) {
                    ContentUriUtils.getFilePath(this, uri.first())?.let {
                        viewmodel.file.postValue(it)
                        val file = File(it)
                        binding.materiUploadLayout.icPdf.visibility = View.VISIBLE
                        binding.materiUploadLayout.uploadFileInfo.text =
                            "${file.name} (ukuran: ${
                                viewmodel.fileUtils.getStringSizeLengthFile(
                                    file.length()
                                )
                            })"
                    }

                }
            }
            IntentUtil.RC_FILE_PICKER -> {
                pickFilesFactory?.handleActivityResult(requestCode, resultCode, data, object :
                    PickFilesStatusCallback {
                    override fun onFilePicked(fileData: ArrayList<FileData>) {
                        fileData.firstOrNull()?.let {
                            viewmodel.file.postValue(it.filePath)
                            binding.materiUploadLayout.icPdf.visibility = View.VISIBLE
                            binding.materiUploadLayout.uploadFileInfo.text =
                                "${it.file?.name} (ukuran: ${
                                    viewmodel.fileUtils.getStringSizeLengthFile(
                                        it.file?.length() ?: 0
                                    )
                                })"
                        }
                    }

                    override fun onPickFileCanceled() {
                    }

                    override fun onPickFileError(errorModel: ErrorModel) =
                        toast(getString(errorModel.errorMessage))
                })

            }
            else -> super.onActivityResult(requestCode, resultCode, data)
        }
    }
}