package id.onklas.app.pages.theory

import android.annotation.SuppressLint
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import id.onklas.app.R
import id.onklas.app.databinding.MateriDetailPageBinding
import id.onklas.app.di.component
import id.onklas.app.pages.Privatepage

class MateriDetailPage : Privatepage() {

    private val binding by lazy { MateriDetailPageBinding.inflate(layoutInflater) }
    private val viewmodel by viewModels<TheoryViewModel> { component.theoryVmFactory }

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(binding.root)
        setSupportActionBar(binding.toolbar)
        supportActionBar?.apply {
            setDisplayShowHomeEnabled(true)
            setDisplayHomeAsUpEnabled(true)

            binding.toolbar.setNavigationOnClickListener { finish() }
            binding.toolbar.title = intent.getStringExtra("title") ?: "Detail Materi"
            title = intent.getStringExtra("title") ?: "Detail Materi"
        }

        val paramMateriId = if (intent.hasExtra("id")) intent.getIntExtra("id", 0) else 0
        val paramSubjectId = if (intent.hasExtra("subId")) intent.getIntExtra("subId", 0) else 0
        if (paramSubjectId > 0 && paramMateriId > 0) {
            loading(msg = "menampilkan detail materi")
            viewmodel.detailMateri(paramMateriId, paramSubjectId).observe(this, { item ->
                dismissloading()
                val uri = Uri.parse(item.file_path.replace("\\", ""))

                val size = if (item.file_size.isEmpty()) 0L else item.file_size.toLong()
                binding.materiLayout.name.text =
                    "${item.file_name} | ${viewmodel.fileUtils.getStringSizeLengthFile(size)}"

                binding.materiLayout.btnLihat.setOnClickListener {
                    viewmodel.intentUtil.openPdf(this@MateriDetailPage, uri.toString(), item.name)
                }

                binding.materiLayout.btnDownload.visibility = View.GONE
//                binding.materiLayout.btnDownload.setOnClickListener {
//                    viewmodel.intentUtil.downloadFile(
//                        this@MateriDetailPage,
//                        uri,
//                        "${item.file_name}.pdf",
//                        "materi"
//                    )
//                }
            })
        } else MaterialAlertDialogBuilder(this, R.style.DialogTheme)
            .setTitle("Materi Tidak tersedia")
            .setMessage("Halaman yang kamu cari sudah tidak tersedia")
            .setCancelable(false)
            .setPositiveButton("Tutup") { dialog, which ->
                dialog.dismiss()
                supportFinishAfterTransition()
            }
            .show()
    }
}