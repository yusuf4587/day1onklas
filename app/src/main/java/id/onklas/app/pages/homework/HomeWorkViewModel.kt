package id.onklas.app.pages.homework

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.toLiveData
import com.github.kittinunf.fuel.core.FileDataPart
import com.github.kittinunf.fuel.core.extensions.jsonBody
import com.github.kittinunf.fuel.coroutines.awaitObjectResult
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.fuel.httpPost
import com.github.kittinunf.fuel.httpUpload
import com.github.kittinunf.fuel.moshi.moshiDeserializerOf
import com.squareup.moshi.Moshi
import id.onklas.app.api.ApiService
import id.onklas.app.db.MemoryDB
import id.onklas.app.db.OnKlasDbUtil
import id.onklas.app.pages.theory.MapelTable
import id.onklas.app.pages.theory.TeacherItem
import id.onklas.app.utils.*
import kotlinx.coroutines.launch
import timber.log.Timber
import java.io.File
import javax.inject.Inject

class HomeWorkViewModel @Inject constructor(
    val pref: PreferenceClass,
    val moshi: Moshi,
    val db: MemoryDB,
    val dbUtil: OnKlasDbUtil,
    val fileUtils: FileUtils,
    val intentUtil: IntentUtil,
    val apiService: ApiService,
    val apiWrapper: ApiWrapper
) : ViewModel() {

    val urlApi by lazy { pref.getString("url_api") }
    private val pageSize = 10
    val errorString by lazy { MutableLiveData<String>() }
    val teacher by lazy {
        try {
            moshi.adapter(TeacherItem::class.java).fromJson(pref.getString("teacher"))
        } catch (e: Exception) {
            null
        }
    }

    val filterTime by lazy { MutableLiveData<String>("Semua") }
    val filterMapel by lazy { MutableLiveData<ArrayList<MapelTable>>(ArrayList()) }
    val classId by lazy { MutableLiveData<Int>() }
    val mapel by lazy { MutableLiveData<Int>() }

    private val anyAdapter by lazy { moshi.adapter(Any::class.java) }
    private val hwAdapter by lazy { moshi.adapter(HomeworkResponse::class.java) }

    val listBelum by lazy {
        db.homework().getHomeworkBelum().toLiveData(
            pageSize, boundaryCallback = PagedListBoundaryCallback(
                { viewModelScope.launch { fetchHomeworkTodo() } },
                {
                    viewModelScope.launch {
                        val count = db.homework().countHomeworkBelum()
                        if (count >= pageSize && hasNextBelum) fetchHomeworkTodo(count)
                    }
                }
            )
        )
    }

    private var hasNextBelum = true
    var prefBelum = -1
    suspend fun fetchHomeworkTodo(start: Int = 0) {
        if (prefBelum == start)
            return

        prefBelum = start
        try {
            val response = apiService.studentTaskTodo(pageSize, start)
//                "${urlApi}mobile/student/assignment-todo"
//                .httpGet(
//                    listOf(
//                        "take" to pageSize,
//                        "skip" to start
//                    )
//                )
//                .awaitObjectResult(moshiDeserializerOf(hwAdapter))
//                .get()
            hasNextBelum = dbUtil.processHomeworkRespones(response)
        } catch (e: Exception) {
            Timber.e(e)
            errorString.postValue(e.localizedMessage)
        }
    }

    val listSudah by lazy {
        db.homework().getHomeworkSudah().toLiveData(
            pageSize, boundaryCallback = PagedListBoundaryCallback(
                { viewModelScope.launch { fetchHomeworkSudah() } },
                {
                    viewModelScope.launch {
                        val count = db.homework().countHomeworkSudah()
                        if (count >= pageSize && hasNextSudah) fetchHomeworkSudah(count)
                    }
                }
            )
        )
    }

    private var hasNextSudah = true
    var prefSudah = -1
    suspend fun fetchHomeworkSudah(start: Int = 0) {
        if (prefSudah == start)
            return

        prefSudah = start
        try {
            val response = apiService.studentTaskDone(pageSize, start)
//                "${urlApi}mobile/student/assignment-done"
//                .httpGet(
//                    listOf(
//                        "take" to pageSize,
//                        "skip" to start
//                    )
//                )
//                .awaitObjectResult(moshiDeserializerOf(hwAdapter))
//                .get()
            hasNextSudah = dbUtil.processHomeworkRespones(response, 1)
        } catch (e: Exception) {
            Timber.e(e)
            errorString.postValue(e.localizedMessage)
        }
    }

    val listNilai by lazy {
        db.homework().getHomeworkNilai().toLiveData(
            pageSize, boundaryCallback = PagedListBoundaryCallback(
                { viewModelScope.launch { fetchHomeworkNilai() } },
                {
                    viewModelScope.launch {
                        val count = db.homework().countHomeworkNilai()
                        if (count >= pageSize && hasNextNilai) fetchHomeworkNilai(count)
                    }
                }
            )
        )
    }

    private var hasNextNilai = true
    var prefNilai = -1
    suspend fun fetchHomeworkNilai(start: Int = 0) {
        if (prefNilai == start)
            return

        prefNilai = start
        try {
            val response = apiService.studentTaskScored(pageSize, start)
//            "${urlApi}mobile/student/assignment-scored"
//                .httpGet(
//                    listOf(
//                        "take" to pageSize,
//                        "skip" to start
//                    )
//                )
//                .awaitObjectResult(moshiDeserializerOf(hwAdapter))
//                .get()
            hasNextNilai = dbUtil.processHomeworkRespones(response, 2)
        } catch (e: Exception) {
            Timber.e(e)
            errorString.postValue(e.localizedMessage)
        }
    }

    fun listTeacher() = (
            if (mapel.value == null && classId.value == null)
                db.homework().getHomeworkTeacher(teacher?.id ?: 0)
            else if (mapel.value != null && classId.value == null)
                db.homework().getHomeworkTeacher(teacher?.id ?: 0, mapel.value ?: 0)
            else if (mapel.value == null && classId.value != null)
                db.homework().getHomeworkTeacherClass(teacher?.id ?: 0, classId.value ?: 0)
            else
                db.homework().getHomeworkTeacherSubjectClass(
                    teacher?.id ?: 0,
                    mapel.value ?: 0,
                    classId.value ?: 0
                )
            ).toLiveData(
            pageSize, boundaryCallback = PagedListBoundaryCallback(
                { viewModelScope.launch { fetchHomeworkTeacher() } },
                {
                    viewModelScope.launch {
                        val count = db.homework().countHomeworkTeacher(teacher?.id ?: 0)
                        if (count >= pageSize && hasNextTeacher) fetchHomeworkTeacher(count)
                    }
                }
            )
        )

    private var hasNextTeacher = true
    var prefTeacher = -1
    suspend fun fetchHomeworkTeacher(start: Int = 0) {
        if (prefTeacher == start)
            return

        prefTeacher = start
        try {
            val response = //apiWrapper.teacherAssignment(pageSize, start, teacher?.id ?: 0, classId.value, mapel.value)
                "${urlApi}mobile/teacher/school-assignment"
                .httpGet(
                    arrayListOf(
                        "take" to pageSize,
                        "skip" to start,
                        "filter[0][0]" to "teacher_id",
                        "filter[0][1]" to "=",
                        "filter[0][2]" to teacher?.id
                    ).apply {
                        classId.value?.let {
                            addAll(
                                listOf(
                                    "filter[1]" to "and",
                                    "filter[2][0]" to "school_classes_id",
                                    "filter[2][1]" to "=",
                                    "filter[2][2]" to it
                                )
                            )
                        }
                        mapel.value?.let {
                            addAll(
                                listOf(
                                    "filter[3]" to "and",
                                    "filter[4][0]" to "school_subject_id",
                                    "filter[4][1]" to "=",
                                    "filter[4][2]" to it
                                )
                            )
                        }
                    }
                )
                .awaitObjectResult(moshiDeserializerOf(hwAdapter))
                .get()
            hasNextTeacher = dbUtil.processHomeworkRespones(response)
        } catch (e: Exception) {
            Timber.e(e)
            errorString.postValue(e.localizedMessage)
        }
    }

    val listDikumpulkan by lazy {
        db.homework().getHomeworkCollected().toLiveData(
            pageSize, boundaryCallback = PagedListBoundaryCallback(
                { viewModelScope.launch { fetchDikumpulkan() } },
                {
                    viewModelScope.launch {
                        val count = db.homework().countHomeworkCollected()
                        if (count >= pageSize && hasNextDikumpulkan) fetchDikumpulkan(count)
                    }
                }
            )
        )
    }

    private var hasNextDikumpulkan = true
    var prefDikumpulkan = -1
    suspend fun fetchDikumpulkan(start: Int = 0) {
        if (prefDikumpulkan == start)
            return

        prefDikumpulkan = start
        try {
            val response = apiService.assignmentCollected(pageSize, start)
//                "${urlApi}mobile/teacher/school-assignment-collected"
//                .httpGet(
//                    listOf(
//                        "take" to pageSize,
//                        "skip" to start
//                    )
//                )
//                .awaitObjectResult(moshiDeserializerOf(HomeworkCollectedResponse::class.java))
//                .get()
            db.homework().insertHomeworkCollected(response.data)
            hasNextDikumpulkan = true
        } catch (e: Exception) {
            Timber.e(e)
            errorString.postValue(e.localizedMessage)
        }
    }

    suspend fun getAssigmentDetail(id: Int): AssignmentResponse? = try {
        apiService.assignmentDetail(id)
//        "${urlApi}mobile/teacher/school-assignment-collected/$id"
//            .httpGet()
//            .awaitObjectResult(moshiDeserializerOf(moshi.adapter(AssignmentResponse::class.java)))
//            .get()
    } catch (e: Exception) {
        Timber.e(e)
        errorString.postValue(e.localizedMessage)
        null
    }

    suspend fun setAssignmentScore(colledtedId: Int, assignmentId: Int, score: Int): Boolean = try {
        apiWrapper.scoreAssignment(colledtedId, assignmentId, score)
//        "${urlApi}mobile/teacher/school-assignment-collected/${colledtedId}/assignment/${assignmentId}"
//            .httpPost()
//            .jsonBody(anyAdapter.toJson(mapOf("score" to score)))
//            .awaitObjectResult(moshiDeserializerOf(moshi.adapter(AssignmentResponse::class.java)))
//            .get()
        true
    } catch (e: Exception) {
        Timber.e(e)
        errorString.postValue(e.localizedMessage)
        false
    }

    suspend fun getDetailHomework(id: Int): HomeworkItemTable? = try {
        db.homework().getDetailHomework(id)
    } catch (e: Exception) {
        Timber.e(e)
        null
    }

    suspend fun uploadHomework(id: Int): Boolean = try {
        val collect = "${urlApi}mobile/student/school-assignment/${id}/collect"
            .httpPost()
            .jsonBody(anyAdapter.toJson(mapOf("checked" to 1)))
            .awaitObjectResult(moshiDeserializerOf(moshi.adapter(CollectHomeworkResponse::class.java)))
            .get()

        tugasFile.value?.let { filepath ->
            "${urlApi}mobile/student/school-assignment/${id}/collect/${collect.data.id}/file"
                .httpUpload()
                .add { FileDataPart(File(filepath), name = "file") }
                .awaitObjectResult(moshiDeserializerOf(anyAdapter))
        }
        db.homework().getDetailHomework(id)?.let {
            db.homework().deleteHomework(it.homework)
        }
        true
    } catch (e: Exception) {
        Timber.e(e)
        errorString.postValue(e.localizedMessage)
        false
    }

    val tugasFile by lazy { MutableLiveData<String>() }
}