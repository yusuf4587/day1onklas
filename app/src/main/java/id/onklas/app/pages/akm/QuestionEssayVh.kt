package id.onklas.app.pages.akm

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.onklas.app.databinding.AkmQuestionEssayItemBinding

class QuestionEssayVh(
    parent: ViewGroup,
    private val onUpdate: (answer: String, item: QuestionAnswers) -> Unit,
    private val onImageClick: (path: String) -> Unit = {},
    val binding: AkmQuestionEssayItemBinding = AkmQuestionEssayItemBinding.inflate(
        LayoutInflater.from(parent.context), parent, false
    )
) : QuestionAdapter.QuestionViewHolder(binding.root) {

    //    private var timer: Timer? = null
    override fun bind(item: QuestionAnswers) {
        binding.item = item
        binding.image.setOnClickListener { onImageClick.invoke(item.question.file_path) }

        // hide input essay temporarily
        binding.layoutEssay.visibility = View.GONE
        onUpdate.invoke("", item)

//        binding.inputEssay.doAfterTextChanged {
//            timer?.cancel()
//            timer = Timer()
//            timer?.schedule(object : TimerTask() {
//                override fun run() {
//                    onUpdate.invoke(binding.inputEssay.text.toString(), item)
//                }
//            }, 500)
//        }

        binding.executePendingBindings()
    }

    override fun update(update: QuestionAnswers) {
//        binding.item = update
//        binding.executePendingBindings()
    }
}