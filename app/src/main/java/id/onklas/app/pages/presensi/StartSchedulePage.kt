package id.onklas.app.pages.presensi

import android.app.ProgressDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import id.onklas.app.R
import id.onklas.app.databinding.StartSchedulePageBinding
import id.onklas.app.di.component
import id.onklas.app.pages.PageDialogFragment
import kotlinx.coroutines.launch

class StartSchedulePage(val scheduleId: Int, val onDismiss: () -> Unit) : PageDialogFragment() {

    private val viewmodel by activityViewModels<PresensiViewModel> { component.presensiVmFactory }
    private lateinit var binding: StartSchedulePageBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? =
        StartSchedulePageBinding.inflate(inflater, container, false).also { binding = it }.root

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        binding.toolbar.setNavigationOnClickListener { dismiss() }
        binding.inPassword.doAfterTextChanged {
            binding.btnConfirm.isEnabled = it.toString().isNotEmpty()
        }

        binding.btnConfirm.setOnClickListener {
            MaterialAlertDialogBuilder(requireContext(), R.style.DialogTheme)
                .setTitle("Konfirmasi")
                .setMessage("Mulai kelas dengan batas keterlambatan ${binding.sbTime.progress + 5} menit?")
                .setPositiveButton("Mulai") { dialog, _ ->
                    lifecycleScope.launch {
                        dialog.dismiss()
                        val progress = ProgressDialog.show(requireContext(), "", "memulai kelas")
                        val res = viewmodel.startClass(
                            scheduleId,
                            binding.inPassword.text.toString(),
                            binding.sbTime.progress + 5
                        )
                        progress.dismiss()
                        if (res) {
                            onDismiss.invoke()
                            dismiss()
                        }
                    }
                }
                .setNeutralButton("Batal") { dialog, _ -> dialog.dismiss() }
                .show()
        }
    }
}